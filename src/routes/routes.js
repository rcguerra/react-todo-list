import React from 'react';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import NotFound from '../containers/NotFound';
import Board from '../screens/Board';

const Routes = () => {
    return(
        <Switch>
            <Route exact path="/">
                <Board />
            </Route>
            <Route component={NotFound} />
        </Switch>
    )
}

export default Routes