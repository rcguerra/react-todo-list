import React from "react"
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Avatar from '@material-ui/core/Avatar';
import { useSelector } from "react-redux";
import { ReducerType } from "../../store/reducers";
import { getUser } from "../../store/reducers/userReducers"
import RightButtons from "./RightButtons";
import NavbarGeneric from "../generic/NavbarGeneric";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    iconButton: {
    },
    title: {
      flexGrow: 1,
      fontWeight: "bold",
      fontSize: 18,
      padding: 8
    },
    small: {
      width: theme.spacing(4),
      height: theme.spacing(4),
    },
  }),
);

export interface NavBarProps {

}

const NavBar: React.FC<NavBarProps> = () => {
  const classes = useStyles();
  const user = useSelector((state: ReducerType) => getUser(state.userReducer))

  return (
    <NavbarGeneric style={{ padding: 0 }}>
      <IconButton edge="start" color="inherit" aria-label="menu" className={classes.iconButton}>
        <Avatar className={classes.small} variant={"square"} src={require("../../assets/img/logo_sm.png")} />
      </IconButton>
      <Typography variant="h6" className={classes.title}>
        TASK MANAGER
          </Typography>
      <div id="right-icons">
        <RightButtons user={user} />
      </div>
    </NavbarGeneric>
  );
}

export default NavBar;