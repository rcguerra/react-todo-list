import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { UserReducerType } from '../../store/reducers/userReducers';
import Avatar from '@material-ui/core/Avatar';
import SocialMediaButton from './SocialMediaButton';
import Typography from '@material-ui/core/Typography';
import { CustomColors } from '../../utils/ColorUtils';

const useStyles = makeStyles(theme => ({
  root: {
    width: 350,
    display: 'flex',
    justifyContent: "center",
    flexDirection: "column",
    borderRadius: 8,
    paddingBottom: 8,
    margin: "0 auto",
    position: "relative",
    top: 150
  },
  titleContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    padding: 2,
    borderBottom: "1px solid " + CustomColors.borderColor
  },
  cardContent: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: 8,
    paddingBottom: "0px !important"
  },
  title: {
    fontWeight: "bold",
    flexGrow: 1,
    fontSize: 14,
    marginLeft: 8,
    color: theme.palette.secondary.contrastText,
  },
  logo: {
    height: 32,
    width: 32
  },
  btnSocial: {
    width: "100%"
  }
}));

export interface LoginFormProps {
  userState: UserReducerType
  onLogin: (e: React.MouseEvent) => void
}

const LoginForm: React.FC<LoginFormProps> = (props) => {
  const { userState, onLogin } = props
  const classes = useStyles()
  return (
    <Card className={classes.root} variant="outlined">
      <div className={classes.titleContainer}>
        <Avatar
          variant={"square"}
          className={classes.logo}
          src={require("../../assets/img/logo_sm.png")}
        />
        <Typography className={classes.title}>LOGIN - TASK MANAGER</Typography>
      </div>
      <CardContent className={classes.cardContent}>
        <SocialMediaButton onClick={(e) => onLogin(e)} userState={userState} />
      </CardContent>
    </Card>
  );
}

export default LoginForm;