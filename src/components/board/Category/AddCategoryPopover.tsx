import React, { useRef } from 'react'
import TextField from '@material-ui/core/TextField';
import PopoverGeneric from '../../generic/PopoverGeneric';
import { makeStyles, PopoverOrigin } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { shadeColor } from '../../../utils/ColorUtils';
import { Check } from '@material-ui/icons';
import { CustomColors } from '../../../utils/ColorUtils';
import CategoryModel from '../../../models/CategoryModel';

const useStyles = makeStyles(theme => ({
    container: {
        display: "flex",
        flexDirection: "column",
        padding: 8
    },
    contentInputs: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },
    colorPicker: {
        height: 36,
        marginLeft: 4,
    },
    contentButtons: {
        marginTop: 4,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
    },
    button: {
        color: "#FFF",
        fontWeight: "bold",
        width: "100%",
        padding: 2
    },
    save: {
        background: CustomColors.addColor,
        marginLeft: 2,
        '&:hover': {
            background: shadeColor(CustomColors.addColor, -25)
        }
    },
    cancel: {
        background: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,
        marginRight: 2,
        '&:hover': {
            background: shadeColor(theme.palette.primary.main, -25)
        }
    }
}))

export interface AddCategoryPopoverProps {
    id: string,
    project: string,
    anchorEl: HTMLElement,
    anchorOrigin?: PopoverOrigin,
    transformOrigin?: PopoverOrigin,
    onAddCategory: (project: CategoryModel) => void,
    onCancel: () => void
}

const AddCategoryPopover: React.FC<AddCategoryPopoverProps> = (props) => {
    const { id, anchorEl, project, anchorOrigin, transformOrigin, onCancel, onAddCategory } = props
    const classes = useStyles()

    const categoryNameRef = useRef<any>()
    const refColor = useRef<any>()

    const handleAddProject = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        if (categoryNameRef.current && refColor.current) {
            const category: CategoryModel = {
                id: "-1",
                name: categoryNameRef.current.value,
                position: 0,
                color: refColor.current.value,
                project: project
            }
            onAddCategory(category)
        }
    }

    return (
        <PopoverGeneric
            id={id}
            open
            anchorEl={anchorEl}
            onCancel={onCancel}
            anchorOrigin={anchorOrigin}
            transformOrigin={transformOrigin}
            clickAwayDetection
        >
            <form className={classes.container} onSubmit={handleAddProject}>
                <div className={classes.contentInputs}>
                    <TextField
                        inputRef={categoryNameRef}
                        id="tf-popover-category-name"
                        label="Category name"
                        variant="outlined"
                        size="small"
                        autoFocus
                        required
                    />
                    <input
                        id="in-popover-color-category"
                        type="color"
                        ref={refColor}
                        className={classes.colorPicker}
                        required
                    />
                </div>

                <div className={classes.contentButtons}>
                    <Button
                        variant="contained"
                        className={`${classes.button} ${classes.cancel}`}
                        onClick={onCancel}
                    >
                        Cancel
                    </Button>
                    <Button
                        variant="contained"
                        className={`${classes.button} ${classes.save}`}
                        startIcon={<Check />}
                        type="submit"
                    >
                        Add
                    </Button>
                </div>
            </form>
        </PopoverGeneric>
    );
}

export default AddCategoryPopover;