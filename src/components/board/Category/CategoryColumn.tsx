import React, { useRef, useState } from 'react'
import { Droppable } from "react-beautiful-dnd";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import { Grow, makeStyles, Tooltip } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { AddOutlined, Check, Close, CreateOutlined, DeleteOutline } from '@material-ui/icons';
import TodoTasksListContainer from '../../../containers/TodoTasksListContainer';
import { shadeColor } from '../../../utils/ColorUtils';
import { Draggable } from "react-beautiful-dnd";
import CategoryModel from '../../../models/CategoryModel';
import { DROPPABLE_TASK } from '../../../utils/dnd/dndTypes';
import { CustomColors } from '../../../utils/ColorUtils';
import TaskModel from '../../../models/TaskModel';
import { SelectedPopoverProps } from '../../generic/PopoverGeneric';
import { ProjectModel } from '../../../models/ProjectModel';


const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        justifyContent: "center",
        flexDirection: "column",
        width: 335,
        borderRadius: 4,
        marginLeft: 8,
        marginRight: 8,
        background: "#fafafa",
        border: `1px solid ${CustomColors.borderColor}`
    },
    titleContainer: (props: CategoryModel) => ({
        display: "flex",
        borderBottom: "2px solid " + shadeColor(props.color, -40),
        padding: 8,
        textAlign: "left",
        fontSize: 14,
        color: props.color,
        background: "#fff",
    }),
    titleContent: {
        maxWidth: 175,
        overflowY: "auto",
        whiteSpace: "nowrap"
    },
    typography: {
        fontWeight: "bold",
    },
    typographyNumberTasks: (props: CategoryModel) => ({
        paddingRight: 12,
        paddingLeft: 12,
        borderRadius: 14,
        marginLeft: 16,
        background: props.color,
        color: shadeColor(props.color, 150)
    }),
    cardContainer: {
        height: 612,
        padding: 8,
        overflowY: "auto",
        WebkitOverflowScrolling: "touch",
    },
    cardContent: {
        padding: 0,
        "&:last-child": {
            paddingBottom: 0
        }
    },
    rightButtons: {
        marginLeft: "auto",
    },
    iconButton: {
        padding: 0,
    },
    addButton: {
        '&:hover': {
            color: CustomColors.addColor
        }
    },
    editButton: (props: CategoryModel) => ({
        '&:hover': {
            color: props.color
        }
    }),
    deleteButton: {
        '&:hover': {
            color: CustomColors.deleteColor
        }
    },
    editContainer: {
        display: "flex",
        flexDirection: "row",
    },
    colorPicker: {
        marginLeft: 2,
        padding: 0
    }
}))

export interface CategoryColumnProps {
    category: CategoryModel,
    tasks: TaskModel[],
    index: number,
    addTaskSelected: SelectedPopoverProps | null,
    setAddTaskSelected: (value: SelectedPopoverProps | null) => void,
    onAddTask: (id: string, anchorEl: HTMLElement) => void
    onEdit: (category: CategoryModel) => void
    onDelete: (id: string, tasks: TaskModel[], anchorEl: HTMLElement) => void
}

const CategoryColumn: React.FC<CategoryColumnProps> = (props) => {
    const { category, tasks, index, onAddTask, onEdit, onDelete } = props
    const classes = useStyles(category)
    const [editCategory, setEditCategory] = useState<CategoryModel | null>(null)

    const refName = useRef<any>()
    const refColor = useRef<any>()

    const handleEditCategory = () => {
        setEditCategory(category)
    }

    const handleCancelEditCategory = () => {
        setEditCategory(null)
    }

    const handleSaveEditCategory = () => {
        if (editCategory) {
            const category = { ...editCategory }
            if (refColor && refColor.current) {
                category.color = refColor.current.value
            }
            if (refName && refName.current) {
                category.name = refName.current.value
            }
            onEdit(category)
            setEditCategory(null)
        }
    }

    const handleAddTask = (event: React.MouseEvent<HTMLElement>) => {
        onAddTask(category.id, event.currentTarget)
    }

    const handleDeleteCategory = (event: React.MouseEvent<HTMLElement>) => {
        onDelete(category.id, tasks, event.currentTarget)
    }

    return (
        <Draggable draggableId={(category.id + 100).toString()} index={index}>
            {provided => (
                <Grow
                    in={true}
                    timeout={1000 * (index + 1)}
                >
                    <Card
                        className={classes.root}
                        variant="outlined"
                        key={category.id}
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                    >
                        <div
                            className={classes.titleContainer}
                            {...provided.dragHandleProps}
                        >
                            {
                                editCategory ?
                                    <div className={classes.editContainer}>
                                        <input
                                            id="name"
                                            type="text"
                                            defaultValue={editCategory.name}
                                            ref={refName} />
                                        <input
                                            id="color"
                                            type="color"
                                            className={classes.colorPicker}
                                            defaultValue={category.color}
                                            ref={refColor}
                                        />
                                    </div>
                                    :
                                    <>
                                        <div className={classes.titleContent}>
                                            <Typography className={classes.typography}>
                                                {category.name}
                                            </Typography>
                                        </div>
                                        <Typography className={classes.typographyNumberTasks}>
                                            {tasks.length}
                                        </Typography>
                                    </>
                            }
                            <div className={classes.rightButtons}>
                                {
                                    editCategory ?
                                        <>
                                            <IconButton
                                                className={`${classes.iconButton} ${classes.editButton}`}
                                                aria-label="save"
                                                onClick={handleSaveEditCategory}
                                            >
                                                <Check />
                                            </IconButton>
                                            <IconButton
                                                className={`${classes.iconButton} ${classes.deleteButton}`}
                                                aria-label="cancel"
                                                onClick={handleCancelEditCategory}
                                            >
                                                <Close />
                                            </IconButton>
                                        </>
                                        :
                                        <>
                                            <Tooltip title="Add Task" arrow>
                                                <IconButton
                                                    className={`${classes.iconButton} ${classes.addButton}`}
                                                    aria-label="add"
                                                    onClick={handleAddTask}
                                                >
                                                    <AddOutlined />
                                                </IconButton>
                                            </Tooltip>
                                            <Tooltip title="Edit Category" arrow>
                                                <IconButton
                                                    className={`${classes.iconButton} ${classes.editButton}`}
                                                    aria-label="edit"
                                                    onClick={handleEditCategory}
                                                >
                                                    <CreateOutlined />
                                                </IconButton>
                                            </Tooltip>
                                            <Tooltip title="Delete Category" arrow>
                                                <IconButton
                                                    className={`${classes.iconButton} ${classes.deleteButton}`}
                                                    aria-label="delete"
                                                    onClick={handleDeleteCategory}
                                                >
                                                    <DeleteOutline />
                                                </IconButton>
                                            </Tooltip>
                                        </>
                                }
                            </div>
                        </div>

                        <Droppable droppableId={category.id.toString()} type={DROPPABLE_TASK}>
                            {(provided) => (
                                <div
                                    className={classes.cardContainer}
                                    {...provided.droppableProps}
                                    ref={provided.innerRef}
                                >
                                    <CardContent className={classes.cardContent}>
                                        <TodoTasksListContainer
                                            tasks={tasks}
                                        />
                                    </CardContent>
                                    {provided.placeholder}
                                </div>
                            )}
                        </Droppable>
                    </Card >
                </Grow>
            )}
        </Draggable >
    );
}

export default CategoryColumn;