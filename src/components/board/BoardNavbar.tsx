import React from 'react'
import IconButton from '@material-ui/core/IconButton';
import ProjectSelectorContainer from '../../containers/selectors/ProjectSelectorContainer'
import NavbarGeneric from '../generic/NavbarGeneric'
import AddBoxRoundedIcon from '@material-ui/icons/AddBoxRounded';
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core';
import { ListAlt, Settings } from '@material-ui/icons';
import { shadeColor, CustomColors } from '../../utils/ColorUtils';
import { DRAWER_WIDTH } from '../generic/DrawerGeneric';

const useStyles = makeStyles(theme => ({
    content: {
        display: "flex",
        flexDirection: "row",
        marginLeft: 16,
        alignItems: "center",
        flexGrow: 1,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    contentShifted: {
        marginRight: DRAWER_WIDTH,
    },
    leftButtons: {
        display: "flex",
        flexGrow: 1
    },
    rightButtons: {
        marginRight: 16
    },
    icon: {
        fontSize: 32,
        color: "#2f9006",
        "&:hover": {
            color: shadeColor("#2f9006", -25)
        }
    },
    editProjectIcon: {
        fontSize: 32,
    },
    addCategoryButton: {
        background: theme.palette.secondary.light,
        "&:hover": {
            background: shadeColor(theme.palette.secondary.light, -25)
        }
    }
}))

export interface BoardNavbarProps {
    project: string | null,
    shifted?: boolean
    onAddProject: (anchorEl: HTMLElement) => void,
    onAddCategory: (anchorEl: HTMLElement) => void,
    onEditProject: () => void
}

const BoardNavbar: React.FC<BoardNavbarProps> = (props) => {
    const { project, shifted, onAddProject, onAddCategory, onEditProject } = props
    const classes = useStyles()

    const handleAddProject = (event: React.MouseEvent<HTMLElement>) => {
        onAddProject(event.currentTarget)
    }
    const handleAddCategory = (event: React.MouseEvent<HTMLElement>) => {
        onAddCategory(event.currentTarget)
    }
    const handleEditProject = () => {
        onEditProject()
    }

    return (
        <NavbarGeneric
            style={{
                background: CustomColors.lightBackground,
                color: "#000",
                borderBottom: `1px solid ${CustomColors.borderColor}`,
            }}
        >
            <div className={`${classes.content} ${shifted && classes.contentShifted}`}>
                <div className={classes.leftButtons}>
                    <ProjectSelectorContainer />
                    <Tooltip title="Add Project" arrow>
                        <IconButton
                            style={{ marginLeft: 4, padding: 0 }}
                            aria-label="add"
                            onClick={handleAddProject}
                        >
                            <AddBoxRoundedIcon className={classes.icon} />
                        </IconButton>
                    </Tooltip>
                </div>
                <div className={classes.rightButtons}>
                    {project &&
                        <>
                            <Tooltip title="Add Category" arrow>
                                <Button
                                    variant="contained"
                                    color="secondary"
                                    className={classes.addCategoryButton}
                                    startIcon={<ListAlt />}
                                    onClick={handleAddCategory}
                                >
                                    Add
                            </Button>
                            </Tooltip>
                            <Tooltip title="Edit Project" arrow>
                                <IconButton
                                    style={{ marginLeft: 16, padding: 0 }}
                                    aria-label="add"
                                    onClick={handleEditProject}
                                >
                                    <Settings className={classes.editProjectIcon} />
                                </IconButton>
                            </Tooltip>
                        </>
                    }
                </div>
            </div>
        </NavbarGeneric >
    );
}

export default BoardNavbar;