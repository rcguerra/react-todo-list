import React, { useMemo, useRef, useState } from 'react'
import { ProjectModel } from '../../../models/ProjectModel';
import TextField from '@material-ui/core/TextField';
import { Check } from '@material-ui/icons';
import DeleteIcon from '@material-ui/icons/Delete';
import DrawerGeneric, { Anchor } from '../../generic/DrawerGeneric';
import { Button, makeStyles } from '@material-ui/core';
import UserModel from '../../../models/UserModel';
import AutoCompleteGeneric from './AutoCompleteInvitees'
import { CustomColors, shadeColor } from '../../../utils/ColorUtils';
import { SelectedPopoverProps } from '../../generic/PopoverGeneric';


const useStyles = makeStyles(theme => ({
    formContainer: {
        display: "flex",
        flexDirection: "column",
        width: "100%",
        height: "100%",
        marginTop: 8
    },
    buttonsContainer: {
        marginTop: 8,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
    },
    button: {
        color: "#FFF",
        fontWeight: "bold",
        width: "100%",
        padding: 2
    },
    save: {
        background: CustomColors.addColor,
        marginLeft: 2,
        '&:hover': {
            background: shadeColor(CustomColors.addColor, -25)
        }
    },
    cancel: {
        background: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,
        marginRight: 2,
        '&:hover': {
            background: shadeColor(theme.palette.primary.main, -25)
        }
    },
    delete: {
        display: "flex",
        marginTop: 16,
        background: CustomColors.deleteColor,
        '&:hover': {
            background: shadeColor(CustomColors.deleteColor, -25)
        }
    }
}))

export interface EditProjectDrawerProps {
    open: boolean,
    anchor: Anchor,
    project: ProjectModel,
    users: UserModel[],
    onDeleteProject: (id: string, anchorEl: HTMLElement) => void,
    onSaveEdit: (project: ProjectModel) => void,
    onChangeOpenState: (open: boolean) => void
}

const EditProjectDrawer: React.FC<EditProjectDrawerProps> = (props) => {
    const { open, anchor, project, users, onChangeOpenState, onSaveEdit, onDeleteProject } = props
    const classes = useStyles()
    const [invitees, setInvitees] = useState<UserModel[]>([])

    const projectNameRef = useRef<any>()

    useMemo(() => {
        if (users.length > 0) {
            const inviteesPopulated: UserModel[] = project.invitees.map((uid: string) => {
                return users.find(user => user.uid === uid)!
            }).filter(value => value !== undefined)
            setInvitees(inviteesPopulated)
        }
    }, [JSON.stringify(users), JSON.stringify(project.invitees)])


    const handleSaveEdit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        const projectAux: ProjectModel = {
            ...project,
            name: projectNameRef.current.value,
            invitees: invitees.map(invitee => invitee.uid)
        }
        onSaveEdit(projectAux)
    }

    const handleDeleteProject = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        event.preventDefault()
        onDeleteProject(project.id, event.currentTarget)
    }

    return (
        <DrawerGeneric
            title={"Project setup"}
            open={open}
            anchor={anchor}
            onChangeOpenState={onChangeOpenState}
        >
            {project &&
                <form className={classes.formContainer} onSubmit={handleSaveEdit}>
                    <TextField
                        inputRef={projectNameRef}
                        id="tf-drawer-name-project"
                        label="Project name"
                        variant="outlined"
                        size="small"
                        defaultValue={project.name}
                        autoFocus
                        required
                    />

                    <AutoCompleteGeneric users={users} invitees={invitees} style={{ marginTop: 8 }} onSetInvitees={setInvitees} />

                    <div className={classes.buttonsContainer}>
                        <Button
                            variant="contained"
                            className={`${classes.button} ${classes.cancel}`}
                            onClick={() => onChangeOpenState(false)}
                        >
                            Cancel
                        </Button>
                        <Button
                            variant="contained"
                            className={`${classes.button} ${classes.save}`}
                            startIcon={<Check />}
                            type="submit"
                        >
                            Save
                        </Button>
                    </div>
                    <Button
                        variant="contained"
                        className={`${classes.button} ${classes.delete}`}
                        startIcon={<DeleteIcon />}
                        type="submit"
                        onClick={handleDeleteProject}
                    >
                        DELETE PROJECT
                    </Button>
                </form>
            }
        </DrawerGeneric >
    );
}

export default EditProjectDrawer;