import React, { useRef } from 'react'
import TextField from '@material-ui/core/TextField';
import PopoverGeneric from '../../generic/PopoverGeneric';
import { makeStyles } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { shadeColor } from '../../../utils/ColorUtils';
import { Check } from '@material-ui/icons';
import { CustomColors } from '../../../utils/ColorUtils';
import { ProjectModel } from '../../../models/ProjectModel';
import UserModel from '../../../models/UserModel';

const useStyles = makeStyles(theme => ({
    container: {
        display: "flex",
        flexDirection: "column",
        padding: 8
    },
    contentButtons: {
        marginTop: 4,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",

    },
    button: {
        color: "#FFF",
        fontWeight: "bold",
        width: "100%",
        padding: 2
    },
    save: {
        background: CustomColors.addColor,
        marginLeft: 2,
        '&:hover': {
            background: shadeColor(CustomColors.addColor, -25)
        }
    },
    cancel: {
        background: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,
        marginRight: 2,
        '&:hover': {
            background: shadeColor(theme.palette.primary.main, -25)
        }
    }
}))

export interface AddProjectPopoverProps {
    id: string,
    user: UserModel,
    anchorEl: HTMLElement,
    open: boolean,
    onAddProject: (project: ProjectModel) => void,
    onCancel: () => void
}

const AddProjectPopover: React.FC<AddProjectPopoverProps> = (props) => {
    const { id, anchorEl, user, open, onCancel, onAddProject } = props
    const classes = useStyles()
    const projectNameRef = useRef<any>()

    const handleAddProject = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        if (projectNameRef.current) {
            const project: ProjectModel = {
                id: "-1",
                name: projectNameRef.current.value,
                createdBy: user.uid,
                invitees: []
            }
            onAddProject(project)
        }
    }

    return (
        <PopoverGeneric
            id={id}
            open={open}
            anchorEl={anchorEl}
            onCancel={onCancel}
            clickAwayDetection
        >
            <form className={classes.container} onSubmit={handleAddProject}>
                <TextField
                    inputRef={projectNameRef}
                    id="tf-popover-name-project"
                    label="Project name"
                    variant="outlined"
                    size="small"
                    autoFocus
                    required
                />

                <div className={classes.contentButtons}>
                    <Button
                        variant="contained"
                        className={`${classes.button} ${classes.cancel}`}
                        onClick={onCancel}
                    >
                        Cancel
                    </Button>
                    <Button
                        variant="contained"
                        className={`${classes.button} ${classes.save}`}
                        startIcon={<Check />}
                        type="submit"
                    >
                        Create
                    </Button>
                </div>
            </form>
        </PopoverGeneric>
    );
}

export default AddProjectPopover;