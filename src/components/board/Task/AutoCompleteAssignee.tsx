import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Avatar from '@material-ui/core/Avatar';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { CSSProperties } from '@material-ui/core/styles/withStyles';
import Typography from '@material-ui/core/Typography';
import UserModel from '../../../models/UserModel';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            padding: "0 !important"
        },
        smallIcon: {
            width: theme.spacing(3),
            height: theme.spacing(3),
            marginRight: 4
        },
        typography: {
            fontSize: "0.8rem"
        }
    }),
);

interface AutoCompleteAssigneeProps {
    assignableUsers: UserModel[],
    assignee: UserModel | null,
    style?: CSSProperties,
    onSetAssignee: (assignee: UserModel | null) => void
}

const AutoCompleteAssignee: React.FC<AutoCompleteAssigneeProps> = (props) => {
    const { assignableUsers, onSetAssignee, assignee, style } = props
    const classes = useStyles()

    const handleChange = (options: UserModel | null) => {
        onSetAssignee(options)
    }

    return (
        <Autocomplete
            id="autocomplete-assignee-outlined"
            size="small"
            className={classes.container}
            autoComplete
            options={assignableUsers}
            getOptionLabel={option => (option.email)}
            value={assignee}
            onChange={(event: any, option: UserModel | null) => handleChange(option)}
            filterSelectedOptions
            renderOption={option =>
                <React.Fragment>
                    <Avatar className={classes.smallIcon} variant={"circle"} src={option.photoURL} />
                    <Typography className={classes.typography}>{option.email}</Typography>
                </React.Fragment>
            }
            renderInput={(params) => (
                <TextField
                    {...params}
                    variant="outlined"
                    label="Assignee"
                />
            )}
            style={style}
        />
    );
}

export default AutoCompleteAssignee