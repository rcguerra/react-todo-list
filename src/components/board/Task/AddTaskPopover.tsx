import React, { useRef } from 'react'
import TextField from '@material-ui/core/TextField';
import PopoverGeneric from '../../generic/PopoverGeneric';
import { makeStyles, PopoverOrigin, TextareaAutosize } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { shadeColor } from '../../../utils/ColorUtils';
import { Check } from '@material-ui/icons';
import { CustomColors } from '../../../utils/ColorUtils';
import TaskModel from '../../../models/TaskModel';

const useStyles = makeStyles(theme => ({
    container: {
        display: "flex",
        flexDirection: "column",
        padding: 8
    },
    textArea: {
        marginTop: 4,
    },
    contentButtons: {
        marginTop: 4,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
    },
    button: {
        color: "#FFF",
        fontWeight: "bold",
        width: "100%",
        padding: 2
    },
    save: {
        background: CustomColors.addColor,
        marginLeft: 2,
        '&:hover': {
            background: shadeColor(CustomColors.addColor, -25)
        }
    },
    cancel: {
        background: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,
        marginRight: 2,
        '&:hover': {
            background: shadeColor(theme.palette.primary.main, -25)
        }
    }
}))

export interface AddTaskPopoverProps {
    id: string,
    category: string,
    anchorEl: HTMLElement,
    open: boolean,
    anchorOrigin?: PopoverOrigin,
    transformOrigin?: PopoverOrigin,
    onAddTask: (project: TaskModel) => void,
    onCancel: () => void
}

const AddTaskPopover: React.FC<AddTaskPopoverProps> = (props) => {
    const { id, anchorEl, category, anchorOrigin, transformOrigin, open, onCancel, onAddTask } = props
    const classes = useStyles()

    const taskNameRef = useRef<any>()
    const descriptionRef = useRef<any>()

    const handleAddTask = () => {
        const task: TaskModel = {
            id: "-1",
            name: taskNameRef.current.value,
            position: 0,
            description: descriptionRef.current.value,
            category
        }
        onAddTask(task)
    }

    return (
        <PopoverGeneric
            id={id}
            open={open}
            anchorEl={anchorEl}
            onCancel={onCancel}
            anchorOrigin={anchorOrigin}
            transformOrigin={transformOrigin}
            clickAwayDetection
        >
            <form className={classes.container} onSubmit={handleAddTask}>
                <TextField
                    inputRef={taskNameRef}
                    id={"tf-task--name-" + category}
                    label="Task name"
                    variant="outlined"
                    size="small"
                    autoFocus
                    required
                />
                <TextareaAutosize
                    className={classes.textArea}
                    ref={descriptionRef}
                    id={"ta-popover-project-description-" + category}
                    placeholder="Description..."
                    rowsMin={3}
                />

                <div className={classes.contentButtons}>
                    <Button
                        variant="contained"
                        className={`${classes.button} ${classes.cancel}`}
                        onClick={onCancel}
                    >
                        Cancel
                    </Button>
                    <Button
                        variant="contained"
                        className={`${classes.button} ${classes.save}`}
                        startIcon={<Check />}
                        type="submit"
                    >
                        Add
                    </Button>
                </div>
            </form>
        </PopoverGeneric>
    );
}

export default AddTaskPopover;