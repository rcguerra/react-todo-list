import React from 'react'
import Menu, { MenuProps } from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { ExitToAppOutlined, PersonOutline } from '@material-ui/icons';
import { CreateOutlined, DeleteOutline } from '@material-ui/icons';

import { makeStyles, withStyles } from '@material-ui/core';
import { CustomColors } from '../../../utils/ColorUtils';

const StyledMenu = withStyles({
    list: {
        padding: 0,
    },
    paper: {
        border: '1px solid #d3d4d5',
        padding: 0
    },
})((props: MenuProps) => (
    <Menu
        autoFocus={false}
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'left',
        }}
        {...props}
    />
));

const useStyles = makeStyles(theme => ({
    editIcon: {
        color: theme.palette.primary.main
    },
    deleteIcon: {
        color: CustomColors.deleteColor
    },
}))

export interface TodoTaskMenuProps {
    anchorEl: HTMLElement | null
    onEdit: () => void
    onDelete: () => void
    onClose: () => void
}

const TodoTaskMenu: React.FC<TodoTaskMenuProps> = (props) => {
    const { anchorEl, onEdit, onDelete, onClose } = props
    const classes = useStyles()
    return (
        <StyledMenu
            id="customized-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={onClose}
        >
            <MenuItem onClick={onEdit} >
                <ListItemIcon>
                    <CreateOutlined fontSize="small" className={classes.editIcon} />
                </ListItemIcon>
                <ListItemText primary="Edit" />
            </MenuItem>
            <MenuItem onClick={onDelete}>
                <ListItemIcon>
                    <DeleteOutline fontSize="small" className={classes.deleteIcon} />
                </ListItemIcon>
                <ListItemText primary="Delete" />
            </MenuItem>
        </StyledMenu >
    );
}

export default TodoTaskMenu;