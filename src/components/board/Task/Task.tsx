import React, { useState } from 'react'
import { Draggable } from "react-beautiful-dnd";
import Card from '@material-ui/core/Card';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import ArrowDropDownOutlinedIcon from '@material-ui/icons/ArrowDropDownOutlined';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core';
import TaskModel from '../../../models/TaskModel';
import UserModel from '../../../models/UserModel';

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "row",
        borderRadius: 4,
        padding: 8,
        marginBottom: 8,
    },
    leftContent: {
        display: "flex",
        flexDirection: "column",
        flexGrow: 1
    },
    title: {
        fontSize: 12,
        fontWeight: "bold",
        color: "#000",
        width: 240,
        overflow: "auto"
    },
    description: {
        fontSize: 14,
        marginTop: 4,
        color: theme.palette.text.secondary,
        height: 45,
        width: 240,
        overflow: "auto"
    },
    rightContent: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    iconButton: {
        padding: 0
    },
    icon: {
    },
    avatar: {
        background: theme.palette.primary.main,
        height: theme.spacing(4),
        width: theme.spacing(4),
        marginTop: 8,
    }
}))

export interface TaskProps {
    item: TaskModel,
    assignee: UserModel | null,
    onClickOptions: (event: React.MouseEvent<HTMLElement>) => void
}

const Task: React.FC<TaskProps> = (props) => {
    const classes = useStyles()
    const { item, assignee, onClickOptions } = props
    return (
        <>
            <div className={classes.leftContent}>
                <Typography className={classes.title}>
                    {item.name}
                </Typography>
                {item.description &&
                    <Typography className={classes.description}>
                        {item.description}
                    </Typography>
                }
            </div>
            <div className={classes.rightContent}>
                <Tooltip title="Task Options" arrow>
                    <IconButton
                        aria-label="options"
                        className={classes.iconButton}
                        onClick={onClickOptions}
                    >
                        <ArrowDropDownOutlinedIcon className={classes.icon} />
                    </IconButton>
                </Tooltip>
                <Tooltip title={assignee ? assignee.displayName : "Not assigned"} arrow>
                    <Avatar alt="Remy Sharp" src={assignee?.photoURL} className={classes.avatar}>
                        {assignee?.displayName[0]}
                    </Avatar>
                </Tooltip>
            </div>
        </>
    )
}

export default Task;