import React, { useRef, useState } from 'react'
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles, TextareaAutosize, TextField } from '@material-ui/core';
import TaskModel from '../../../models/TaskModel';
import { Check, CloseOutlined } from '@material-ui/icons';
import { CustomColors, shadeColor } from "../../../utils/ColorUtils"
import UserModel from '../../../models/UserModel';
import AutoCompleteAssignee from './AutoCompleteAssignee';

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "row",
        width: "100%"
    },
    leftContent: {
        display: "flex",
        flexDirection: "column",
        flexGrow: 1,
        marginRight: 4
    },
    textArea: {
        marginTop: 6,
        resize: "vertical",
        border: "1px solid #c4c4c4"
    },
    rightContent: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    buttonsContainer: {
        display: "flex",
        flexDirection: "row"
    },
    iconButton: {
        padding: 0,
    },
    cancelButton: {
        color: CustomColors.deleteColor,
        "&:hover": {
            color: shadeColor(CustomColors.deleteColor, -25)
        }
    },
    acceptButton: {
        marginLeft: 4,
        color: CustomColors.addColor,
        "&:hover": {
            color: shadeColor(CustomColors.addColor, -25)
        }
    },
    icon: {
        fontSize: "1.3rem"
    },
    avatar: {
        background: theme.palette.primary.main,
        height: theme.spacing(4),
        width: theme.spacing(4),
        marginTop: 8,
    }
}))

export interface EditTaskFormProps {
    item: TaskModel,
    assignee: UserModel | null,
    assignableUsers: UserModel[],
    onCancel: () => void,
    onEditTask: (task: TaskModel) => void
}

const EditTaskForm: React.FC<EditTaskFormProps> = (props) => {
    const { item, assignee, assignableUsers, onCancel, onEditTask } = props
    const [assigneeAux, setAssigneeAux] = useState<UserModel | null>(assignee)
    const classes = useStyles()
    const refName = useRef<any>()
    const refDescription = useRef<any>()

    const handleEditTask = () => {
        const task: TaskModel = {
            ...item,
            name: refName.current.value,
            description: refDescription.current.value,
            assignee: assigneeAux?.uid
        }
        onEditTask(task)
    }

    return (
        <form onSubmit={handleEditTask} className={classes.root}>
            <div className={classes.leftContent}>
                <TextField
                    id={`tf-edit-task-name`}
                    defaultValue={item.name}
                    inputRef={refName}
                    label="Task name"
                    variant="outlined"
                    size="small"
                    autoFocus
                    required
                />
                <AutoCompleteAssignee
                    assignee={assigneeAux}
                    onSetAssignee={setAssigneeAux}
                    assignableUsers={assignableUsers}
                    style={{ marginTop: 6 }}
                />
                <TextareaAutosize
                    id={`ta-edit-task-description`}
                    className={classes.textArea}
                    defaultValue={item.description}
                    ref={refDescription}
                    placeholder="Description..."
                    rowsMin={3}
                />
            </div>
            <div className={classes.rightContent}>
                <div className={classes.buttonsContainer}>
                    <IconButton
                        aria-label="cancel"
                        className={`${classes.iconButton} ${classes.cancelButton}`}
                        onClick={onCancel}
                    >
                        <CloseOutlined className={classes.icon} />
                    </IconButton>
                    <IconButton
                        aria-label="save"
                        className={`${classes.iconButton} ${classes.acceptButton}`}
                        type="submit"
                    >
                        <Check className={classes.icon} />
                    </IconButton>
                </div>
                <Tooltip title={assigneeAux ? assigneeAux.displayName : "Not assigned"} arrow>
                    <Avatar alt="Remy Sharp" src={assigneeAux?.photoURL} className={classes.avatar}>
                        {assigneeAux?.displayName[0]}
                    </Avatar>
                </Tooltip>
            </div>
        </form>
    );
}

export default EditTaskForm;