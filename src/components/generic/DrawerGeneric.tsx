import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import Typography from '@material-ui/core/Typography';

export const DRAWER_WIDTH = 300;
const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    appBar: {
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        width: `calc(100% - ${DRAWER_WIDTH}px)`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginRight: DRAWER_WIDTH,
    },
    drawer: {
        width: DRAWER_WIDTH,
        flexShrink: 0,
    },
    drawerPaper: {
        width: DRAWER_WIDTH,
        top: 'unset'
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-start',
    },
    icon: {
        padding: 0
    },
    drawerBody: {
        display: "flex",
        flexDirection: "column",
        padding: 8,
        alignItems: "center",
    }
}))

export type Anchor = 'top' | 'left' | 'bottom' | 'right';

interface DrawerGeneric {
    title: string,
    anchor: Anchor,
    open: boolean,
    onChangeOpenState: (open: boolean) => void
}

const DrawerGeneric: React.FC<DrawerGeneric> = (props) => {
    const classes = useStyles();
    const { anchor, open, title, onChangeOpenState } = props

    const handleSetOpenState = () => {
        onChangeOpenState(false)
    }

    return (
        <Drawer
            className={classes.drawer}
            variant="persistent"
            anchor={anchor}
            open={open}
            classes={{
                paper: classes.drawerPaper,
            }}
        >
            <div className={classes.drawerHeader}>
                <IconButton className={classes.icon} onClick={handleSetOpenState}>
                    {anchor === 'left' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                </IconButton>

                <Typography>{title.toUpperCase()}</Typography>
            </div>
            <Divider />
            <div className={classes.drawerBody}>
                {props.children}
            </div>
        </Drawer >
    );
}
export default DrawerGeneric
