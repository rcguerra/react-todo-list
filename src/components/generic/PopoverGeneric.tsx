import React from 'react';
import Popover, { PopoverOrigin } from '@material-ui/core/Popover';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

export interface SelectedPopoverProps {
    id: string,
    anchorEl: HTMLElement,
}

interface PopoverGenericProps {
    id: string,
    open: boolean,
    anchorEl: HTMLElement,
    anchorOrigin?: PopoverOrigin,
    transformOrigin?: PopoverOrigin,
    clickAwayDetection?: boolean
    onCancel: () => void
}

const PopoverGeneric: React.FC<PopoverGenericProps> = (props) => {
    const { id, open, anchorEl, anchorOrigin, transformOrigin, clickAwayDetection, onCancel } = props
    return (
        <Popover
            id={id}
            open={open}
            anchorEl={anchorEl}
            anchorOrigin={anchorOrigin ? anchorOrigin : {
                vertical: 'bottom',
                horizontal: 'center',
            }}
            transformOrigin={transformOrigin ? transformOrigin : {
                vertical: 'top',
                horizontal: 'left',
            }}
        >
            <ClickAwayListener onClickAway={clickAwayDetection ? onCancel : () => { }}>
                {props.children}
            </ClickAwayListener>
        </Popover>
    );
}

export default PopoverGeneric