import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import GenericModel from '../../models/GenericModel';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            minWidth: 120,
        },
        selectEmpty: {
        },
    }),
);

export interface SelectorGenericProps<T> {
    label: string,
    values: T[],
    selectedValue: string | null,
    onChangeValue: (item: string) => void
}

const SelectorGeneric = <T extends GenericModel>(props: SelectorGenericProps<T>) => {
    const { label, values, selectedValue, onChangeValue } = props
    const classes = useStyles()
    const noneValue = "-1"

    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        onChangeValue(event.target.value as string);
    };

    return (
        <FormControl variant="outlined" className={classes.formControl} size={"small"}>
            <InputLabel id="demo-simple-select-outlined-label">{label}</InputLabel>
            <Select
                labelId="demo-simple-select-outlined-label"
                id="generic-selector"
                value={selectedValue ? selectedValue : noneValue}
                label={label}
                onChange={handleChange}
            >
                <MenuItem value={noneValue}>
                    <em>None</em>
                </MenuItem>
                {
                    values.map(value => (
                        <MenuItem key={value.id} value={value.id}>
                            {value.name}
                        </MenuItem>
                    ))
                }
            </Select>
        </FormControl>
    );
}

export default SelectorGeneric;