import GenericModel from "./GenericModel"
import UserModel from "./UserModel";

export interface ProjectModel extends GenericModel {
    createdBy: string,
    invitees: string[]
}

export interface ProjectModelPopulated extends GenericModel {
    createdBy: string,
    invitees: UserModel[]
}