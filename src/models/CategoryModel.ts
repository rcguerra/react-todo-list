import DraggableModel from "./DraggableModel";
import GenericModel from "./GenericModel"

export default interface CategoryModel extends GenericModel, DraggableModel {
    project: string,
    color: string,
}