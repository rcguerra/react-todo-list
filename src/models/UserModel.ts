export default interface UserModel {
    uid: string,
    displayName: string,
    photoURL: string,
    email: string
}