import DraggableModel from "./DraggableModel";
import GenericModel from "./GenericModel";

export default interface TaskModel extends GenericModel, DraggableModel {
    description?: string,
    category: string,
    assignee?: string | null
}