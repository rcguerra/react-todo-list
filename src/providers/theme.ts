import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';

export default responsiveFontSizes(createMuiTheme({
    palette: {
        primary: {
            main: '#5362e3',
            light: '#484848',
            dark: '#000000',
            contrastText: '#FFFFFF',
        },
        secondary: {
            main: '#c0c0c0',
            light: '#fafafa',
            dark: '#757575',
            contrastText: "#403939"
        },
        text: {
            primary: "#565656",
            secondary: "#757575"
        },
    },
    mixins: {
        toolbar: {
            height: 40,
            padding: 8,
            background: "none"
        }
    }

})); 