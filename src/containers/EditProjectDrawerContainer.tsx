import React, { useEffect, useMemo, useState } from 'react'
import { useSelector } from 'react-redux';
import EditProejctDrawer from "../components/board/Project/EditProjectDrawer"
import { Anchor } from '../components/generic/DrawerGeneric';
import PopoverDeleteConditional from '../components/generic/PopoverDeleteConditional';
import { SelectedPopoverProps } from '../components/generic/PopoverGeneric';
import useProjects from '../firebase/firestore/useProjects';
import useUsers from '../firebase/firestore/useUsers';
import useSearchParams from '../hooks/useSearchParams';
import { ProjectModel, ProjectModelPopulated } from '../models/ProjectModel';
import UserModel from '../models/UserModel';
import { ReducerType } from '../store/reducers';
import { getUser } from '../store/reducers/userReducers';

export interface EditProjectDrawerContainerProps {
    anchor: Anchor,
    open: boolean,
    onChangeOpenState: (open: boolean) => void
}

const EditProjectDrawerContainer: React.FC<EditProjectDrawerContainerProps> = (props) => {
    const { anchor, open, onChangeOpenState } = props
    const { searchParams, history } = useSearchParams()

    const user = useSelector((state: ReducerType) => getUser(state.userReducer))
    const { projectsData } = useProjects(user?.uid)
    const { updateProject, deleteProject } = useProjects()
    const { usersData } = useUsers()

    const [project, setProject] = useState<ProjectModel | null>(null)
    const [availableUsers, setAvailableUsers] = useState<UserModel[]>([])
    const [deleteProjectSelected, setDeleteProjectSelected] = useState<null | SelectedPopoverProps>(null)

    const projectId = searchParams.getProjectId()

    useEffect(() => {
        if (projectId) {
            const project = projectsData.data.find(project => project.id === projectId)
            setProject(project ? project : null)
        }
    }, [projectId, JSON.stringify(projectsData.data)])

    useMemo(() => {
        const users = usersData.data.filter(userData => userData.uid !== user?.uid)
        setAvailableUsers(users)
    }, [JSON.stringify(usersData.data)])

    const handleSaveEdit = (project: ProjectModel) => {
        updateProject(project)
        onChangeOpenState(false)
    }

    const handleDeleteProject = (id: string, anchorEl: HTMLElement) => {
        setDeleteProjectSelected({ id, anchorEl })
    }

    const handleCancelDeleteProject = () => {
        setDeleteProjectSelected(null)
    }

    const handleAcceptDeleteProject = () => {
        if (deleteProjectSelected && project) {
            deleteProject(project.id)
        }
        setDeleteProjectSelected(null)
        searchParams.clearProject()
        history.push(searchParams.toString())
    }

    return (
        <>
            {deleteProjectSelected &&
                <PopoverDeleteConditional
                    id={"delete-project"}
                    open={deleteProjectSelected !== null}
                    title={"Delete this project and all related data?"}
                    anchorEl={deleteProjectSelected.anchorEl}
                    onAccept={handleAcceptDeleteProject}
                    onCancel={handleCancelDeleteProject}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                />
            }
            {project &&
                <EditProejctDrawer
                    anchor={anchor}
                    open={open}
                    project={project}
                    users={availableUsers}
                    onSaveEdit={handleSaveEdit}
                    onChangeOpenState={onChangeOpenState}
                    onDeleteProject={handleDeleteProject}
                />
            }
        </>
    );
}

export default EditProjectDrawerContainer;