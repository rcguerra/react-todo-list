import React, { useContext, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { FirebaseContext } from '../../firebase';
import { ReducerType } from '../../store/reducers';
import { loadingUserAction } from "../../store/actions/userActions"
import LoginForm from '../../components/login/LoginForm';
import useUsers from '../../firebase/firestore/useUsers';

export interface LoginContainerProps {

}

const LoginContainer: React.FC<LoginContainerProps> = () => {
    const dispatch = useDispatch()
    const { firebaseController } = useContext(FirebaseContext)
    const { addUser } = useUsers()
    let userState = useSelector((state: ReducerType) => state.userReducer);

    const handleLogin = (e: React.MouseEvent) => {
        firebaseController.login(addUser)
        dispatch(loadingUserAction())
    }

    return (
        <LoginForm onLogin={handleLogin} userState={userState} />
    );
}

export default LoginContainer;