import React, { useEffect, useMemo, useState } from 'react'
import TaskModel from '../models/TaskModel'
import useTasks from '../firebase/firestore/useTasks'
import { ProjectModel } from '../models/ProjectModel'
import useUsers from '../firebase/firestore/useUsers'
import UserModel from '../models/UserModel'
import useSearchParams from '../hooks/useSearchParams'
import useProjects from '../firebase/firestore/useProjects'
import TaskContainer from './TaskContainer'
import { useSelector } from 'react-redux'
import { ReducerType } from '../store/reducers'
import { getUser } from '../store/reducers/userReducers'

interface TodoTasksListContainerProps {
    tasks: TaskModel[]
}

const TodoTasksListContainer: React.FC<TodoTasksListContainerProps> = (props) => {
    const { tasks } = props

    const [assignableUsers, setAssignableUsers] = useState<UserModel[]>([])
    const [project, setProject] = useState<ProjectModel | null>(null)

    const { searchParams } = useSearchParams()
    const { updateTask, deleteTask } = useTasks()
    const { usersData } = useUsers()

    const user = useSelector((state: ReducerType) => getUser(state.userReducer))
    const { projectsData } = useProjects(user?.uid)

    const projectId = searchParams.getProjectId()

    useEffect(() => {
        if (projectId) {
            const selectedProject = projectsData.data.find(dataProject => dataProject.id === projectId)
            if (selectedProject) setProject(selectedProject)
        }
    }, [projectId, JSON.stringify(projectsData.data)])

    useMemo(() => {
        if (project) {
            const usersPopulated: UserModel[] = project.invitees.map((uid: string) => {
                return usersData.data.find(user => user.uid === uid)!
            }).filter(value => value !== undefined)
            if (user) usersPopulated.push(user)
            setAssignableUsers(usersPopulated)
        }
    }, [project, JSON.stringify(usersData.data)])

    const handleDeleteTask = (id: string) => {
        deleteTask(id)
    }

    const handleEditTask = (task: TaskModel) => {
        updateTask(task)
    }

    return (
        <>
            {tasks
                .sort(({ position: previousPosition }, { position: currentPosition }) => previousPosition - currentPosition)
                .map((task, index) =>
                    <TaskContainer key={task.id} index={index} item={task} assignableUsers={assignableUsers} onDeleteTask={handleDeleteTask} onEditTask={handleEditTask} />
                )}
        </>
    )
}


export default TodoTasksListContainer