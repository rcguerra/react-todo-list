import React, { useEffect, useState } from 'react'
import { Draggable } from "react-beautiful-dnd";
import Card from '@material-ui/core/Card';
import { makeStyles } from '@material-ui/core';
import TaskModel from '../models/TaskModel';
import UserModel from '../models/UserModel';
import EditTaskForm from '../components/board/Task/EditTaskForm';
import TodoTaskMenu from '../components/board/Task/TodoTaskMenu';
import Task from '../components/board/Task/Task';

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "row",
        borderRadius: 4,
        padding: 8,
        marginBottom: 8,
    },
    leftContent: {
        display: "flex",
        flexDirection: "column",
        flexGrow: 1
    },
    title: {
        fontSize: 12,
        fontWeight: "bold",
        color: "#000",
        width: 240,
        overflow: "auto"
    },
    description: {
        fontSize: 14,
        marginTop: 4,
        color: theme.palette.text.secondary,
        height: 45,
        width: 240,
        overflow: "auto"
    },
    rightContent: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    iconButton: {
        padding: 0
    },
    icon: {
    },
    avatar: {
        height: theme.spacing(4.5),
        width: theme.spacing(4.5),
        marginTop: 8,
    }
}))

export interface TaskContainerProps {
    item: TaskModel,
    index: number,
    assignableUsers: UserModel[],
    onDeleteTask: (id: string) => void,
    onEditTask: (task: TaskModel) => void
}

const TaskContainer: React.FC<TaskContainerProps> = (props) => {
    const { item, index, assignableUsers, onDeleteTask, onEditTask } = props
    const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null)
    const [isEdit, setIsEdit] = useState<boolean>(false)
    const [assignee, setAssignee] = useState<UserModel | null>(null)
    const classes = useStyles()

    useEffect(() => {
        if (item.assignee) {
            const assigneePopulated = assignableUsers.find(user => user.uid === item.assignee)
            if (assigneePopulated) {
                setAssignee(assigneePopulated)
            }
        }
    }, [item.assignee, assignableUsers])

    const handleClickOptions = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget)
    }

    const handleCloseOptions = () => {
        setAnchorEl(null)
    }

    const handleOnDeleteTask = () => {
        onDeleteTask(item.id)
        setAnchorEl(null)
    }

    const handleEdit = () => {
        setIsEdit(true)
        setAnchorEl(null)
    }
    const handleCancelEdit = () => {
        setIsEdit(false)
    }
    const handleSaveEdit = (item: TaskModel) => {
        onEditTask(item)
        setIsEdit(false)
    }

    return (
        <>
            <Draggable draggableId={item.id.toString()} index={index}>
                {provided => (
                    <Card
                        className={classes.root}
                        variant="outlined"
                        key={item.id}
                        style={{ marginBottom: 60 }}
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                    >
                        {isEdit ?
                            <EditTaskForm
                                item={item}
                                assignee={assignee}
                                onCancel={handleCancelEdit}
                                onEditTask={handleSaveEdit}
                                assignableUsers={assignableUsers}
                            />
                            :
                            <Task
                                item={item}
                                assignee={assignee}
                                onClickOptions={handleClickOptions}
                            />
                        }

                    </Card>

                )}
            </Draggable>

            <TodoTaskMenu anchorEl={anchorEl} onClose={handleCloseOptions} onDelete={handleOnDeleteTask} onEdit={handleEdit} />
        </>
    );
}

export default TaskContainer;