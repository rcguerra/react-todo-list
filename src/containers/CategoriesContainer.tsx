import { makeStyles } from '@material-ui/core';
import React, { useEffect, useMemo, useState } from 'react'
import { DragDropContext, Droppable, DropResult } from "react-beautiful-dnd";
import CategoryColumn from '../components/board/Category/CategoryColumn';
import { SelectedPopoverProps } from '../components/generic/PopoverGeneric'
import PopoverDeleteConditional from '../components/generic/PopoverDeleteConditional';
import AddTaskPopover from '../components/board/Task/AddTaskPopover'
import useCategories from '../firebase/firestore/useCategories';
import CategoryModel from '../models/CategoryModel';
import DraggableModel from '../models/DraggableModel';
import TaskModel from '../models/TaskModel';

import { DROPPABLE_CATEGORY, DROPPABLE_TASK } from '../utils/dnd/dndTypes';
import useTasks from '../firebase/firestore/useTasks';

const useStyles = makeStyles({
    content: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "left",
        overflowY: "auto"
    },
    draggableContainer: {
        display: "flex",
        paddingLeft: 16,
        paddingRight: 16,
    },
})

export interface CategoriesContainerProps {

}

const CategoriesContainer: React.FC<CategoriesContainerProps> = () => {
    const classes = useStyles()

    const { categoriesData, updateCategories, updateCategory, deleteCategory } = useCategories()
    const { tasksData, initDataTasks, addTask, updateTasks, deleteTasks } = useTasks()

    const [categories, setCategories] = useState<CategoryModel[]>(categoriesData.data)
    const [tasks, setTasks] = useState<TaskModel[]>(tasksData.data)
    const [isDragging, setIsDragging] = useState<boolean>(false)
    const [isDraggingTasks, setIsDraggingTasks] = useState<boolean>(false)
    const [deleteCategorySelected, setDeleteCategorySelected] = useState<null | SelectedPopoverProps & { tasks: TaskModel[] }>(null)
    const [addTaskSelected, setAddTaskSelected] = useState<null | SelectedPopoverProps>(null)

    useMemo(() => {
        if (!isDraggingTasks)
            setTasks(tasksData.data)
    }, [JSON.stringify(tasksData.data)])

    useMemo(() => {
        if (!isDragging) {
            setCategories(categoriesData.data)
        }
    }, [JSON.stringify(categoriesData.data)])

    useEffect(() => {
        const projectCategoryIds = categories.map(category => category.id)
        initDataTasks(projectCategoryIds)
    }, [,
        JSON.stringify(categoriesData.data),
        addTaskSelected
    ])

    const onDragEnd = (result: DropResult) => {
        const { source, destination, type } = result

        const orderOnDragSameContainer = <T extends { position: number }>(task: T) => {
            if (destination) {
                if (task.position === destination.index) {
                    if (source.index - destination.index > 0) {
                        task.position++
                    } else {
                        task.position--
                    }
                }
                else if (task.position >= source.index && task.position < destination.index && task.position > 0) {
                    task.position--
                }
                else if (task.position < source.index && task.position > destination.index) {
                    task.position++
                }
            }
            return task
        }
        const orderTasksWhenDragDiferentColumn = (task: TaskModel, sourceColumn: CategoryModel, destinationColumn: CategoryModel) => {
            if (destination) {
                if (task.category === sourceColumn.id) {
                    if (task.position > source.index && task.position > 0) {
                        task.position--
                    }
                }
                else if (task.category === destinationColumn.id) {
                    if (task.position >= destination.index) {
                        task.position++
                    }
                }
            }
            return task
        }

        if (type === DROPPABLE_TASK) {
            // Make sure we have a valid destination
            if (destination === undefined || destination === null)
                return null

            // Make sure we're actually moving the item
            if (source.droppableId === destination.droppableId && destination.index === source.index)
                return null

            // Set start and end variables
            const start = categories.find(category => category.id === source.droppableId)
            const end = categories.find(category => category.id === destination.droppableId)

            const auxTasks = [...tasks]
            const sourceIndex = auxTasks.findIndex(
                task =>
                    start?.id === task.category && task.position === source.index
            )
            // If start is the same as end, we're in the same column
            if (start === end) {
                const orderedTasks: TaskModel[] = []
                auxTasks.forEach(task => {
                    if (task.category === start?.id) {
                        orderedTasks.push(orderOnDragSameContainer(task))
                    }
                })

                const resultArray: TaskModel[] = auxTasks.map(task => {
                    const taskResult = orderedTasks.find(orderedTask => orderedTask.id === task.id)
                    return taskResult ? taskResult : task
                })
                resultArray[sourceIndex] = { ...resultArray[sourceIndex], position: destination.index }

                setIsDraggingTasks(true)
                setTasks(resultArray)
                updateTasks(resultArray, () => { setIsDraggingTasks(false) })

            } else if (start && end) {
                const orderedTasks = auxTasks.map(task => {
                    return orderTasksWhenDragDiferentColumn(task, start, end)
                })
                orderedTasks[sourceIndex] = { ...orderedTasks[sourceIndex], category: destination.droppableId, position: destination.index }
                setIsDraggingTasks(true)
                setTasks(orderedTasks)
                updateTasks(orderedTasks, () => { setIsDraggingTasks(false) })
            }
        }
        else if (type === DROPPABLE_CATEGORY) {
            if (destination) {
                const auxCategories = [...categories]
                const sourceCategoryIndex = auxCategories.findIndex(category => category.position === source.index)
                const resultCategories = auxCategories.map(category => (orderOnDragSameContainer(category)))

                resultCategories[sourceCategoryIndex] = { ...resultCategories[sourceCategoryIndex], position: destination.index }
                setIsDragging(true)
                setCategories(resultCategories)
                updateCategories(resultCategories, () => { setIsDragging(false) })
            }
        }
    }

    function reorderItem<T extends DraggableModel>(task: T, source: T) {
        if (task.position >= source.position && task.position > 0) {
            task.position--
        }
        return task
    }

    //Categories
    const handleEditCategory = (category: CategoryModel) => {
        const categoriesAux = [...categories]
        const index = categories.findIndex(cat => cat.id === category.id)
        categoriesAux[index] = category

        updateCategory(category, setCategories(categoriesAux))
    }

    const handleDeleteCategory = (id: string, tasks: TaskModel[], anchorEl: HTMLElement) => {
        setDeleteCategorySelected({ id, tasks, anchorEl })
    }

    const handleCancelDeleteCategory = () => {
        setDeleteCategorySelected(null)
    }

    const handleAcceptDeleteCategory = () => {
        if (deleteCategorySelected) {
            var categoriesAux = [...categories]
            const index = categoriesAux.findIndex(category => category.id === deleteCategorySelected.id)
            categoriesAux = categoriesAux.map(category => (reorderItem(category, categoriesAux[index])))
            categoriesAux.splice(index, 1)

            setDeleteCategorySelected(null)
            deleteCategory(deleteCategorySelected.id, () => {
                updateCategories(categoriesAux)
                deleteTasks(deleteCategorySelected.tasks)
            })
        }
    }

    //Tasks
    const handleAddTask = (id: string, anchorEl: HTMLElement) => {
        setAddTaskSelected({ id, anchorEl })
    }

    const handleAcceptAddTask = (task: TaskModel) => {
        if (addTaskSelected) {
            const filteredTasks = tasks.filter(t => t.category === task.category)
            const tasksAux = filteredTasks.map(task => ({ ...task, position: task.position += 1 }))

            setAddTaskSelected(null)
            updateTasks(
                tasksAux,
                addTask(task)
            )
        }
    }

    return (
        <div className={classes.content}>
            {deleteCategorySelected &&
                <PopoverDeleteConditional
                    id="delete-category"
                    open={deleteCategorySelected !== null}
                    title={"Delete this category and all related tasks?"}
                    anchorEl={deleteCategorySelected.anchorEl}
                    onAccept={handleAcceptDeleteCategory}
                    onCancel={handleCancelDeleteCategory}
                />
            }
            {addTaskSelected &&
                <AddTaskPopover
                    id="add-task"
                    open={addTaskSelected !== null}
                    category={addTaskSelected.id}
                    anchorEl={addTaskSelected.anchorEl}
                    onAddTask={handleAcceptAddTask}
                    onCancel={() => { setAddTaskSelected(null) }}
                />
            }

            <DragDropContext onDragEnd={onDragEnd}>
                <Droppable droppableId={"category-columns"} type={DROPPABLE_CATEGORY} direction={"horizontal"}>
                    {(provided) => (
                        <div
                            className={classes.draggableContainer}
                            {...provided.droppableProps}
                            ref={provided.innerRef}>
                            {
                                categories
                                    .sort(({ position: previousPosition }, { position: currentPosition }) => previousPosition - currentPosition)
                                    .map((category, index) => {
                                        const columnTasks = tasks.filter(task => task.category === category.id)
                                        return <CategoryColumn
                                            key={category.id}
                                            index={index}
                                            category={category}
                                            tasks={columnTasks}
                                            onEdit={handleEditCategory}
                                            onDelete={handleDeleteCategory}
                                            onAddTask={handleAddTask}
                                            addTaskSelected={addTaskSelected}
                                            setAddTaskSelected={setAddTaskSelected}
                                        />

                                    })
                            }
                            {provided.placeholder}
                        </div>
                    )}
                </Droppable>
            </DragDropContext >
        </div>
    )

}


export default CategoriesContainer;