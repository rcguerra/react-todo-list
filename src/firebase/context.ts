import React, { createContext } from "react";
import firebaseController,{IFirebaseController} from "./FirebaseController"

interface IFirebaseContext {
    firebaseController: IFirebaseController
}

const FirebaseContext = createContext<IFirebaseContext>({firebaseController: firebaseController});

export default FirebaseContext;