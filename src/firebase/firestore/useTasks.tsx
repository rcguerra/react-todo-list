import React, { useEffect, useState } from 'react';
import FirebaseController from '..';
import TaskModel from '../../models/TaskModel';
import GenericData from '../../utils/typeUtils/GenericData';
import { TASKS_COLLECTION, CATEGORIES_COLLECTION, USERS_COLLECTION } from './collectionIds';

function useTasks() {
    const [categoryIds, setCategoryIds] = useState<string[]>([])
    const [tasksData, setTasksData] = React.useState<GenericData<TaskModel>>({
        error: null,
        isFetching: true,
        data: [],
    });

    const collection = FirebaseController
        .db
        .collection(TASKS_COLLECTION)

    useEffect(() => {
        const unsubscribe = collection.onSnapshot(snapshotManager);
        return unsubscribe;
    }, [categoryIds]);

    function initDataTasks(categoryIds: string[]) {
        setCategoryIds(categoryIds)
    }

    function snapshotManager(snapshot: any, error?: Error | null) {
        if (error) {
            setTasksData(
                {
                    error: { name: error.name, message: error.message, status: 400 },
                    isFetching: false,
                    data: [],
                }
            )
        }
        else {
            setTasksData(
                {
                    error: null,
                    isFetching: false,
                    data: snapshot.docs
                        .filter((doc: any) => (
                            doc.data().category && categoryIds.includes(doc.data().category.id)
                        ))
                        .map((doc: any) => {
                            return {
                                id: doc.id,
                                name: doc.data().name,
                                position: doc.data().position,
                                description: doc.data().description,
                                category: doc.data().category?.id,
                                assignee: doc.data().assignee?.id,
                            }
                        }),
                }
            )
        }
    }

    async function getTasks(category: string) {
        return await collection.where(category, "==", category).get()
    }

    function addTask(task: TaskModel) {
        const ref = collection.doc();
        collection.doc(ref.id).set({
            name: task.name,
            position: task.position,
            description: task.description ? task.description : "",
            category: FirebaseController.db.doc(`${CATEGORIES_COLLECTION}/` + task.category),
            assignee: FirebaseController.db.doc(`${USERS_COLLECTION}/` + task.assignee),
        })
    }

    function updateTasks(tasks: TaskModel[], callback?: any) {
        const batch: firebase.firestore.WriteBatch = FirebaseController.db.batch()
        tasks.forEach(task => {
            const ref = collection.doc(task.id)
            batch.update(ref, {
                name: task.name,
                position: task.position,
                description: task.description ? task.description : "",
                category: FirebaseController.db.doc(`${CATEGORIES_COLLECTION}/` + task.category),
                assignee: task.assignee ? FirebaseController.db.doc(`${USERS_COLLECTION}/` + task.assignee) : null,
            })
        })
        batch.commit().then(() => callback && callback())
    }

    function updateTask(task: TaskModel, callback?: any) {
        collection.doc(task.id).update({
            name: task.name,
            position: task.position,
            description: task.description ? task.description : "",
            category: FirebaseController.db.doc(`${CATEGORIES_COLLECTION}/` + task.category),
            assignee: task.assignee ? FirebaseController.db.doc(`${USERS_COLLECTION}/` + task.assignee) : null,
        })
    }

    function deleteTask(id: string, callback?: any) {
        collection.doc(id).delete().then(() => callback && callback())
    }

    function deleteTasks(tasks: TaskModel[], callback?: any) {
        const batch: firebase.firestore.WriteBatch = FirebaseController.db.batch()
        tasks.forEach(task => {
            const ref = collection.doc(task.id)
            batch.delete(ref)
        })
        batch.commit().then(() => { callback && callback() })
    }

    return {
        tasksData,
        initDataTasks,
        getTasks,
        addTask,
        deleteTask,
        deleteTasks,
        updateTasks,
        updateTask
    }
}

export default useTasks;