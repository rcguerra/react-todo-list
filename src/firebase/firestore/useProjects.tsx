import React, { useEffect } from 'react';
import FirebaseController from '..';
import { ProjectModel, ProjectModelPopulated } from '../../models/ProjectModel';
import UserModel from '../../models/UserModel';
import { getUser } from '../../store/reducers/userReducers';
import GenericData from '../../utils/typeUtils/GenericData';
import { PROJECTS_COLLECTION, USERS_COLLECTION } from './collectionIds';
import useUsers from './useUsers';

function useProjects(userId?: string) {
    const [projectsData, setProjectssData] = React.useState<GenericData<ProjectModel>>({
        error: null,
        isFetching: true,
        data: [],
    });
    const { getUserById } = useUsers()

    const collection = FirebaseController
        .db
        .collection(PROJECTS_COLLECTION)
    const usersColelction = FirebaseController
        .db
        .collection(USERS_COLLECTION)

    useEffect(() => {
        const unsubscribe = collection.onSnapshot(snapshotManager);
        return unsubscribe;
    }, []);

    function snapshotManager(snapshot: any, error?: Error | null) {
        if (error) {
            setProjectssData(
                {
                    error: { name: error.name, message: error.message, status: 400 },
                    isFetching: false,
                    data: [],
                }
            )
        }
        else {
            setProjectssData(
                {
                    error: null,
                    isFetching: false,
                    data: snapshot.docs
                        .filter((doc: any) => doc.data().createdBy && doc.data().createdBy.id === userId)
                        .map((doc: any) => {
                            return {
                                id: doc.id,
                                name: doc.data().name,
                                createdBy: doc.data().createdBy.id,
                                invitees: doc.data().invitees.map((invitee: any) => invitee.id),
                            }
                        }),
                }
            )
        }
    }

    async function getProjectByIdPopulated(id: string) {
        return await getProjectById(id).then(result => {
            if (result) {
                return loadInvitees(result.invitees).then((invitees: any) => {
                    const project: ProjectModelPopulated = {
                        ...result,
                        invitees: invitees
                    }
                    return project
                })
            }
        })
    }

    async function loadInvitees(users: any) {
        return await Promise.all(users.map((user: any) => (
            getUserById(user.id)
        )))
    }

    async function getProjectById(id: string) {
        var docRef = collection.doc(id);

        return await docRef.get().then(function (doc) {
            if (doc.exists) {
                let project: ProjectModel | null = null
                const data = doc.data()
                if (data) {
                    project = {
                        id: doc.id,
                        name: data.name,
                        createdBy: data.createdBy && data.createdBy.id,
                        invitees: data.invitees
                    }
                }
                return project
            } else {
                throw new Error("No such document");
            }
        }).catch(function (error) {
            throw new Error("Error getting document");
        });
    }

    function addProject(project: ProjectModel) {
        const ref = collection.doc();
        collection.doc(ref.id).set({
            name: project.name,
            createdBy: FirebaseController.db.doc(`${USERS_COLLECTION}/` + project.createdBy),
            invitees: project.invitees
        })
    }

    function updateProject(project: ProjectModel, callback?: any) {
        collection.doc(project.id).update({
            name: project.name,
            createdBy: FirebaseController.db.doc(`${USERS_COLLECTION}/` + project.createdBy),
            invitees: project.invitees.map(invitee => (
                FirebaseController.db.doc(`${USERS_COLLECTION}/` + invitee)
            ))
        })
    }

    function deleteProject(id: string, callback?: any) {
        collection.doc(id).delete().then(() => callback && callback())
    }

    return {
        projectsData,
        getProjectByIdPopulated,
        addProject,
        updateProject,
        deleteProject
    }
}

export default useProjects;