import React, { useEffect } from 'react';
import FirebaseController from '..';
import useSearchParams from '../../hooks/useSearchParams';
import CategoryModel from '../../models/CategoryModel';
import GenericData from '../../utils/typeUtils/GenericData';
import { CATEGORIES_COLLECTION, PROJECTS_COLLECTION } from './collectionIds';

function useCategories() {
    const { searchParams } = useSearchParams()
    const [categoriesData, setCategoriesData] = React.useState<GenericData<CategoryModel>>({
        error: null,
        isFetching: true,
        data: [],
    });
    const project = searchParams.getProjectId()

    const collection = FirebaseController
        .db
        .collection(CATEGORIES_COLLECTION)

    useEffect(() => {
        const unsubscribe = collection.onSnapshot(snapshotManager);
        return unsubscribe;
    }, [project]);

    function snapshotManager(snapshot: any, error?: Error | null) {
        if (error) {
            setCategoriesData(
                {
                    error: { name: error.name, message: error.message, status: 400 },
                    isFetching: false,
                    data: [],
                }
            )
        }
        else {
            setCategoriesData(
                {
                    error: null,
                    isFetching: false,
                    data: snapshot.docs
                        .filter((doc: any) => doc.data().project && doc.data().project.id === project)
                        .map((doc: any) => {
                            return {
                                id: doc.id,
                                name: doc.data().name,
                                position: doc.data().position,
                                color: doc.data().color,
                                project: doc.data().project.id
                            }
                        }),
                }
            )
        }


    }

    function addCategory(category: CategoryModel) {
        const ref = collection.doc();
        collection.doc(ref.id).set({
            name: category.name,
            position: category.position,
            color: category.color,
            project: FirebaseController.db.doc(`${PROJECTS_COLLECTION}/` + category.project),
        })
    }

    function updateCategories(categories: CategoryModel[], callback?: any) {
        const batch: firebase.firestore.WriteBatch = FirebaseController.db.batch()
        categories.forEach(category => {
            const ref = collection.doc(category.id)
            batch.update(ref, {
                name: category.name,
                position: category.position,
                color: category.color,
                project: FirebaseController.db.doc(`${PROJECTS_COLLECTION}/` + category.project),
            })
        })
        batch.commit().then(() => callback && callback())
    }

    function updateCategory(category: CategoryModel, callback?: any) {
        collection.doc(category.id).update({
            name: category.name,
            position: category.position,
            color: category.color,
            project: FirebaseController.db.doc(`${PROJECTS_COLLECTION}/` + category.project)
        })
    }

    function deleteCategory(id: string, callback?: any) {
        collection.doc(id).delete().then(() => callback && callback())
    }

    return {
        categoriesData,
        addCategory,
        deleteCategory,
        updateCategories,
        updateCategory
    }
}

export default useCategories;