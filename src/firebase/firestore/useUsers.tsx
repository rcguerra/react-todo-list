import React, { useEffect } from 'react';
import FirebaseController from '..';
import UserModel from '../../models/UserModel';
import GenericData from '../../utils/typeUtils/GenericData';
import { USERS_COLLECTION } from './collectionIds';

function useUsers() {
    const [usersData, setUsersData] = React.useState<GenericData<UserModel>>({
        error: null,
        isFetching: true,
        data: [],
    });

    const collection = FirebaseController
        .db
        .collection(USERS_COLLECTION)

    useEffect(() => {
        const unsubscribe = collection.onSnapshot(snapshotManager);
        return unsubscribe;
    }, []);

    function snapshotManager(snapshot: any, error?: Error | null) {
        if (error) {
            setUsersData(
                {
                    error: { name: error.name, message: error.message, status: 400 },
                    isFetching: false,
                    data: [],
                }
            )
        }
        else {
            setUsersData(
                {
                    error: null,
                    isFetching: false,
                    data: snapshot.docs.map((doc: any) => ({
                        uid: doc.id,
                        displayName: doc.data().displayName,
                        photoURL: doc.data().photoURL,
                        email: doc.data().email
                    })),
                }
            )
        }
    }

    function addUser(user: UserModel) {
        collection.doc(user.uid).set({
            displayName: user.displayName,
            photoURL: user.photoURL,
            email: user.email
        })
    }

    async function getUserById(id: string) {
        var docRef = collection.doc(id);

        return await docRef.get().then(function (doc) {
            if (doc.exists) {
                let user: UserModel | null = null
                const data = doc.data()
                if (data) {
                    user = {
                        uid: doc.id,
                        displayName: data.displayName,
                        email: data.email,
                        photoURL: data.photoURL
                    }
                }
                return user
            } else {
                throw new Error("No such document");
            }
        }).catch(function (error) {
            throw new Error("Error getting document");
        });
    }

    return {
        usersData,
        getUserById,
        addUser
    }
}

export default useUsers;