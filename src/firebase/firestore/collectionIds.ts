export const USERS_COLLECTION = "users"
export const PROJECTS_COLLECTION = "projects"
export const CATEGORIES_COLLECTION = "categories"
export const TASKS_COLLECTION = "tasks"