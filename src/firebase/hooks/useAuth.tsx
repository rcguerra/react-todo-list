import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { initUserAction } from "../../store/actions/userActions";

const useAuth = () => {
	const dispatch = useDispatch()

	useEffect(() => {
		dispatch(initUserAction())
	}, []);
};

export default useAuth;
