import firebase from "firebase";

var firebaseConfig = {
    apiKey: "AIzaSyCBP5DU8M2SOJYTwpZlWNO1UOSeVPEwJLY",
    authDomain: "task-manager-72458.firebaseapp.com",
    databaseURL: "https://task-manager-72458.firebaseio.com",
    projectId: "task-manager-72458",
    storageBucket: "task-manager-72458.appspot.com",
    messagingSenderId: "1036029138670",
    appId: "1:1036029138670:web:382766e2c68e9fbb2505ff"
  };

export default firebaseConfig