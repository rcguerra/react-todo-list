import {combineReducers} from "redux"
import userReducer, { UserReducerType } from "./userReducers"

export type ReducerType = {
    userReducer: UserReducerType
}

export default combineReducers<ReducerType>({
    userReducer,
})