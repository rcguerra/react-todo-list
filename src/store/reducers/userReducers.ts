import { Action } from "redux";
import UserModel from "../../models/UserModel";
import HttpError from "../../utils/typeUtils/HttpError";
import { LOADING_USER, LOGIN_USER, LOGOUT_USER } from "../actionTypes/userTypes";

export interface    UserReducerType {
    user: UserModel | null,
    error: HttpError | null,
    loading: boolean
}

const initialState: UserReducerType = 
    {
        user: null, 
        error: null,
        loading: false
    }

export default function (state=initialState, action: any){
    switch(action.type){
        case LOGIN_USER:
            return {
                ...state,
                user: action.payload,
                loading: false
            }
        case LOGOUT_USER:
            return {
                ...state,
                user: action.payload,
                loading: false
            }
        case LOADING_USER:
            return {
                ...state,
                loading: action.payload
            } 
        default:
            return state

    }
}

export const getUser = (state: UserReducerType) => {
    return state.user
}