export function shadeColor(color: string, amount: number) {
    return '#' + color.replace(/^#/, '').replace(/../g, color => ('0' + Math.min(255, Math.max(0, parseInt(color, 16) + amount)).toString(16)).substr(-2));
}

interface ColorTypes {
    addColor: string,
    deleteColor: string,
    borderColor: string,
    lightBackground: string
}

export const CustomColors: ColorTypes = {
    addColor: "#2f9006",
    deleteColor: "#dd014e",
    borderColor: "#eaeaea",
    lightBackground: "#fafafa"
}