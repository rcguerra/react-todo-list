const PROJECT = 'project'

export default class SearchParams extends URLSearchParams {
    //Project
    setProjectId(id: string): SearchParams {
        this.set(PROJECT, id)
        return this
    }

    getProjectId(): string | null {
        return this.get(PROJECT)
    }

    clearProject() {
        this.delete(PROJECT)
        return this
    }

    //QueryStrings methods
    getCategoriesQueryString() {
        const project = this.getProjectId()

        const searchParams = new SearchParams()
        if (project !== null) searchParams.setProjectId(project)

        return searchParams.toString()
    }

    clearOrderBy() {
        this.delete('orderby')
        return this
    }

    toString() {
        return '?' + super.toString()
    }
}
