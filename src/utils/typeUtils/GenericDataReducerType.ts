import HttpError from "./HttpError";

export interface GenericReducerType<T> {
    values: T[] | null;
    isInvalidate: boolean;
    isFetching: boolean;
    error: HttpError | null;
}