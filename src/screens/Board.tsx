import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core';
import BoardNavbarContainer from '../containers/BoardNavbarContainer';
import CategoriesContainer from '../containers/CategoriesContainer';
import EditProjectDrawerContainer from '../containers/EditProjectDrawerContainer';
import useSearchParams from '../hooks/useSearchParams';

const useStyles = makeStyles({
    container: {
        display: "flex",
        flexDirection: "column"
    },
    content: {
        marginTop: 8
    }
})

const Board: React.FC = () => {
    const classes = useStyles()
    const [isEditProject, setIsEditProject] = useState<boolean>(false)
    const { searchParams } = useSearchParams()
    const project = searchParams.getProjectId()

    useEffect(() => {
        setIsEditProject(false)
    }, [project])

    const handleChangeEditProject = (open: boolean) => {
        setIsEditProject(open)
    }

    return (
        <div className={classes.container}>
            <EditProjectDrawerContainer
                open={isEditProject}
                anchor={"right"}
                onChangeOpenState={handleChangeEditProject}
            />
            <BoardNavbarContainer onEditProject={handleChangeEditProject} shifted={isEditProject} />

            <div className={classes.content} >
                <CategoriesContainer />
            </div>
        </div>
    );
}

export default Board;