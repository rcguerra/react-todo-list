import { useHistory, useLocation } from 'react-router-dom';
import SearchParams from '../utils/SearchParams';

const useSearchParams = () => {
    const searchParams = new SearchParams(useLocation().search)
    const history = useHistory()

    return { searchParams, history };
}

export default useSearchParams;
